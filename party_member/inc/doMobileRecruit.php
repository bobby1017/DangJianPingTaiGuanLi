<?php

global $_GPC;
$company_re_id = $_GPC['company_re_id'];
require_once dirname(__FILE__)."/../model/user.php";
require_once dirname(__FILE__)."/../model/api.php";
if(!isUser()){
    $url =$this->createMobileUrl('login');
    header("location: $url");
    setJumpUrl();
    die();
}
$user= getUser();

$request = postCurl(getServer()."/getRecruitByCompanyId",array(
    "companyid"=>$company_re_id
));

if($request['ret']!=0 && $request['ret']!=null){
    message("服务器错误");
}

$data = $request['data'];

/*var_dump($data);*/

include $this->template('recruit');