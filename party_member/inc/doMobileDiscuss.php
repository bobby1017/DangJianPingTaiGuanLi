<?php

global $_GPC;
require_once dirname(__FILE__)."/../model/user.php";
require_once dirname(__FILE__)."/../model/api.php";
if(!isUser()){
    $url =$this->createMobileUrl('login');
    header("location: $url");
    setJumpUrl();
    die();
}

$id = $_GPC['id'];
$type = $_GPC['type'];

include $this->template('discuss');