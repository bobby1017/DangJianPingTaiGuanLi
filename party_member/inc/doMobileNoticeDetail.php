<?php

global $_GPC;
$id = $_GPC['id'];
require_once dirname(__FILE__)."/../model/user.php";
require_once dirname(__FILE__)."/../model/api.php";
if(!isUser()){
    $url =$this->createMobileUrl('login');
    header("location: $url");
    setJumpUrl();
    die();
}
$user= getUser();

$request = postCurl(getServer()."/getInformationById",array(
    "id"=>$id
));

$data = $request['data'];
$item = $data['list'][0];
$host = $request['host'];

/*var_dump($item);*/

include $this->template('noticeDetail');