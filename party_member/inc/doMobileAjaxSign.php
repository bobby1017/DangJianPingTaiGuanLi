<?php
global  $_GPC,$_W;
$id = $_GPC['id'];
$type = $_GPC['type'];

require_once dirname(__FILE__)."/../model/user.php";
require_once dirname(__FILE__)."/../model/api.php";
if(!isUser()){
    $url =$this->createMobileUrl('login');
    header("location: $url");
    setJumpUrl();
    die();
}
$user= getUser();


/*党员活动报名*/
if($type==1){
    $request = postCurl(getServer()."/partyActivitiesSign",array(
        "activitiesid"=>$id,
        "memberid"=>$user['id'],
        "membername"=>$user["name"],
        "partyid"=>$user["partyid"],
        "partyname"=>$user['partyname']
    ));
}
/*党员活动报名取消*/
if($type==2){
    $request = postCurl(getServer()."/partyActivitiesUnSign",array(
        "activitiesid"=>$id,
        "memberid"=>$user['id'],
        "membername"=>$user["name"],
        "partyid"=>$user["partyid"],
        "partyname"=>$user['partyname']
    ));
}

/*志愿活动报名*/
if($type==3){
    $request = postCurl(getServer()."/volunteerSign",array(
        "volunteerid"=>$id,
        "memberid"=>$user['id'],
        "membername"=>$user["name"],
        "partyid"=>$user["partyid"],
        "partyname"=>$user['partyname']
    ));
}

/*志愿活动取消报名*/
if($type==4){
    $request = postCurl(getServer()."/volunteerUnSign",array(
        "volunteerid"=>$id,
        "memberid"=>$user['id'],
        "membername"=>$user["name"],
        "partyid"=>$user["partyid"],
        "partyname"=>$user['partyname']
    ));
}



if($request["ret"]==0){
    echo json_encode(array('status'=>true,'result'=>"操作成功"),true);
    die();
}else{
    echo json_encode(array('status'=>false,'result'=>$request['msg']),true);
    die();
}