<?php
require_once dirname(__FILE__)."/../model/user.php";
require_once dirname(__FILE__)."/../model/api.php";



if(!isUser()){
    $url =$this->createMobileUrl('login');
    header("location: $url");
    setJumpUrl();
    die();
}
$user= getUser();

$request = postCurl(getServer()."/getPartyactivitiesByPartyid",array(
    "partyid"=>$user["partyid"]
));

$data1= $request['data'];



$request2 = postCurl(getServer()."/getVolunteerByPartyid",array(
    "partyid"=> $user["partyid"]
));


$data2= $request2['data'];



include $this->template('activities');